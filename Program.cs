﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea_004
{
    class Program
    {
        static void Main(string[] args)
        {
            Departamentos miDepartamento = new Departamentos();

            int[] montoVenta = new int[7];
            int[] cantEmpleados = new int[7];
            int[] cantClientesMes = new int[7];

            int calculoProm = 0;
            int calculoPromVentas = 0;
            int Max = 0;
            int Min = 999999999;

            string[] ejemploArray = new string[7];
            ejemploArray[0] = "Once"; ejemploArray[1] = "San Nicolas"; ejemploArray[2] = "Almagro";
            ejemploArray[3] = "microcentro2"; ejemploArray[4] = "Palermo"; ejemploArray[5] = "San Cristobal";
            ejemploArray[6] = "Parque Patricios";

            for (int x = 0; x < 1; x++)
            {
                for (int i = 0; i < ejemploArray.Length; i++)
                {
                    Console.WriteLine("Ingrese el monto de la sucursal de " + ejemploArray[i]);
                    montoVenta[x] = int.Parse(Console.ReadLine());
                    if (montoVenta[x] > Max)
                    {
                        Max = montoVenta[x];
                        miDepartamento.ventaMax = Max;
                    }
                    if (montoVenta[x] < Min)
                    {
                        Min = montoVenta[x];
                        miDepartamento.ventaMin = Min;
                    }
                    calculoPromVentas += montoVenta[x];
                }
                for (int i = 0; i < cantEmpleados.Length; i++)
                {
                    Console.WriteLine("Ingrese la cantidad de empleados de " + ejemploArray[i]);
                    cantEmpleados[x] = int.Parse(Console.ReadLine());
                    calculoProm += cantEmpleados[x];
                }
                for (int i = 0; i < cantClientesMes.Length; i++)
                {
                    Console.WriteLine("Cantidad de Clientes que compraron ese mes de " + ejemploArray[i]);
                    cantClientesMes[x] = int.Parse(Console.ReadLine());
                }
                miDepartamento.calcPromEmpleados = calculoProm / 7;
                miDepartamento.calcPromVentas = calculoPromVentas / 7;
                miDepartamento.ventaTotal = calculoPromVentas;
                Console.WriteLine("El promedio de empleados por mes es de " + miDepartamento.calcPromEmpleados);
                Console.WriteLine("El promedio de ventas es de " + miDepartamento.calcPromVentas);
                Console.WriteLine("El MAXIMO de ventas es de " + miDepartamento.ventaMax);
                Console.WriteLine("El MINIMO de ventas es de " + miDepartamento.ventaMin);
            }

        }
    }
}